package org.apache.storm.starter.bolt;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.*;
import java.io.BufferedWriter;
import java.util.Map;
import java.io.FileWriter;
import java.io.IOException;
public class FileWriteBolt extends BaseBasicBolt {
private final String filename = "output.txt";
private BufferedWriter writer;
@Override
public void prepare(Map stormConf, TopologyContext context) {
super.prepare(stormConf, context);
try {
writer = new BufferedWriter(new FileWriter(filename, true));
}catch(IOException e){
  e.printStackTrace();
}
}
@Override
public void execute(Tuple tuple, BasicOutputCollector collector) {
try {
writer.write(tuple.getInteger(0) + "\n");
}catch(IOException e){
  e.printStackTrace();
}
}
@Override
public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {}
@Override
public void cleanup() {
try {
writer.close();
}catch(IOException e){
  e.printStackTrace();
}
}

public static void main(String[] args)
{
System.out.println("This is from FileWriteBolt");
}





}

