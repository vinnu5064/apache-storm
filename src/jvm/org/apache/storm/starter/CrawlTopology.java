package org.apache.storm.starter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.task.ShellBolt;
import org.apache.storm.spout.ShellSpout;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.starter.spout.RandomSentenceSpout;
import org.apache.storm.utils.Utils;
import org.apache.storm.topology.SpoutDeclarer;
import org.apache.storm.topology.BoltDeclarer;

import java.util.HashMap;
import java.util.Map;


public class CrawlTopology {


public static class CrawlSpout extends ShellSpout implements IRichSpout {
public CrawlSpout() {
super("perl", "CrawlSpout.pl");
System.out.println(" ##### This is test message from CrawlSpout Class ##### ");
}
@Override
public void declareOutputFields(OutputFieldsDeclarer declarer) {
declarer.declare(new Fields("word"));
}
@Override
public Map<String, Object> getComponentConfiguration() {
return null;
}
}


public static class CrawlBolt extends ShellBolt implements IRichBolt {
public CrawlBolt() {
super("perl", "CrawlBolt.pl");
System.out.println(" ##### This is test message from CrawlBolt Class ##### ");
}
@Override
public void declareOutputFields(OutputFieldsDeclarer declarer) {
declarer.declare(new Fields("word"));
}
@Override
public Map<String, Object> getComponentConfiguration() {
return null;
}
}

public static void main(String[] args) throws Exception {
System.out.println("##########   This is CrawlTopology in Perl  ##########");
TopologyBuilder builder = new TopologyBuilder();
StormSubmitter stormcluster = new StormSubmitter();
builder.setSpout("url-gen", new CrawlSpout());
builder.setBolt("save-db",new CrawlBolt(), 2).setNumTasks(2).shuffleGrouping("url-gen");
Config conf = new Config();
conf.setDebug(true);
if (args != null && args.length > 0) {
conf.setNumWorkers(1);
StormSubmitter.submitTopologyWithProgressBar(args[0], conf, builder.createTopology());
}
else {
conf.setMaxTaskParallelism(1);
LocalCluster localcluster = new LocalCluster();
localcluster.submitTopology("crawl-topology", conf, builder.createTopology());
Thread.sleep(10000);
    }
  }
}


