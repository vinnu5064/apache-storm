/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.storm.starter;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.task.ShellBolt;
import org.apache.storm.spout.ShellSpout;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.starter.spout.RandomSentenceSpout;

import java.util.HashMap;
import java.util.Map;

import org.apache.storm.topology.SpoutDeclarer;
import org.apache.storm.topology.BoltDeclarer;


/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */
public class WordCountTopology {

public static class TestSpout extends ShellSpout implements IRichSpout {
public TestSpout() {
super("perl", "TestSpout.pl");
System.out.println(" ##### This is Test for TestSpout ##### ");
}

@Override
public void declareOutputFields(OutputFieldsDeclarer declarer) {
declarer.declare(new Fields("word"));
}



@Override
public Map<String, Object> getComponentConfiguration() {
return null;
}
 
}




public static class TestBolt extends ShellBolt implements IRichBolt {
public TestBolt() {
super("perl", "TestBolt.pl");
System.out.println(" ##### This is Test for TestBolt ##### ");
}

@Override
public void declareOutputFields(OutputFieldsDeclarer declarer) {
declarer.declare(new Fields("word"));
}
@Override
public Map<String, Object> getComponentConfiguration() {
return null;
}

}




  public static class WordCount extends BaseBasicBolt {
//    Map<String, Integer> counts = new HashMap<String, Integer>();

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
      String word = tuple.getString(0);
      System.out.println("######################"  + word);
//      Integer count = counts.get(word);
     // if (count == null)
       // count = 0;
      //count++;
  //    counts.put(word, count);
    collector.emit(new Values(word, "0"));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word", "count"));
    }
  }

  public static void main(String[] args) throws Exception {

     TopologyBuilder builder = new TopologyBuilder();
     StormSubmitter cluster = new StormSubmitter();
SpoutDeclarer spout = builder.setSpout("spout", new TestSpout(), 1);
spout.setCPULoad(1);
spout.setMemoryLoad(1024, 1024);

BoltDeclarer bolt1 = builder.setBolt("split", new TestBolt(), 1).shuffleGrouping("spout");
bolt1.setCPULoad(1);

//BoltDeclarer bolt2 = builder.setBolt("bolt", new WordCount(), 1).fieldsGrouping("split", new Fields("word"));
//bolt2.setMemoryLoad(1024);


    Config conf = new Config();
    conf.setDebug(true);

    conf.setTopologyWorkerMaxHeapSize(2048.0);
    conf.setTopologyPriority(29);
    conf.setTopologyStrategy(org.apache.storm.scheduler.resource.strategies.scheduling.DefaultResourceAwareStrategy.class);

    if (args != null && args.length > 0) {
      conf.setNumWorkers(1);

      StormSubmitter.submitTopologyWithProgressBar(args[0], conf, builder.createTopology());
    }
    else {
      conf.setMaxTaskParallelism(1);

//      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("word-count", conf, builder.createTopology());

      Thread.sleep(10000);

    }
  }
}
