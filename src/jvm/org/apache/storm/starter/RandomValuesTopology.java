package org.apache.storm.starter;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;
import org.apache.storm.starter.spout.RandomSpout;
import org.apache.storm.starter.bolt.FileWriteBolt;

public class RandomValuesTopology {

//private static final String name = RandomValuesTopology.class.getName();

public static void main(String[] args) {
System.out.println("##########   This is RandomValuesTopology   ##########");
TopologyBuilder builder = new TopologyBuilder();
builder.setSpout("random-spout", new RandomSpout());
builder.setBolt("writer-bolt",new FileWriteBolt()).shuffleGrouping("random-spout");
Config conf = new Config();
conf.setDebug(true);
conf.setMaxTaskParallelism(3);
LocalCluster cluster = new LocalCluster();
cluster.submitTopology("mytopology", conf, builder.createTopology());
//Utils.sleep(3000000);
//Utils.sleep(3000);
//cluster.killTopology(name);
//cluster.shutdown();
// to run it on a live cluster
//StormSubmitter.submitTopology("topology", conf, builder.createTopology()); 
}
}
