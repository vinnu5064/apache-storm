//import org.apache.storm.spout.ShellSpout;
//import org.apache.storm.topology.IRichSpout;

package org.apache.storm.starter.spout;


import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.utils.Utils;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.Random;
import java.util.Map;


public class RandomSpout extends BaseRichSpout {
private SpoutOutputCollector spoutOutputCollector;
private Random random;
@Override
public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
outputFieldsDeclarer.declare(new Fields("val"));
}
@Override
public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
this.spoutOutputCollector = spoutOutputCollector;
random = new Random();
}
@Override
public void nextTuple() {
spoutOutputCollector.emit( new Values( random.nextInt(100) ) );
}


public static void main(String[] args)
{
System.out.println("This is from RandomSpout");
}
}
