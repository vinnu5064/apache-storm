package SentenceSpout;

use Moo;
use namespace::clean;

extends 'IO::Storm::Spout';

my $sentences = ["a little brown dog",
                 "the man petted the dog",
                 "four score and seven years ago",
                 "an apple a day keeps the doctor away",];
my $num_sentences = scalar(@$sentences);

sub next_tuple {
    my ($self) = @_;

    $self->emit( [ $sentences->[ rand($num_sentences) ] ] );

}

SentenceSpout->new->run;
