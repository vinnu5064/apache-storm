package SplitSentenceBolt;

use Moo;
use namespace::clean;

extends 'IO::Storm::Bolt';

sub process {
    my ($self, $tuple) = @_;

    my @words = split(' ', $tuple->values->[0]);
    foreach my $word (@words) {

        $self->emit([ $word ]);
    }

}

SplitSentenceBolt->new->run;
