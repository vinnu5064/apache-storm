package KafkaTestSpout;
use 5.010;
use strict;
use warnings;
use Scalar::Util qw(blessed);
use Try::Tiny;
use Kafka::Connection;
use Kafka::Consumer;
use WWW::Mechanize;
use Moo;
use namespace::clean;
use LWP::UserAgent;
use HTML::LinkExtor;
use URI;
extends 'IO::Storm::Spout';
$" = "\n";
#my $MAXURLCOUNT = 100; #The max no of unique URLs to look at
#my $COUNT = 0;
#my $BASEURL = $ARGV[0];
#my $MIMEEXTS = "(s?html?|php|xml|asp|pl|css|jpg|gif|pdf)";
 our ( $connection, $consumer, $message, $BASEURL );
    sub next_tuple {
        our ($self) = @_;
        try {
        $connection = Kafka::Connection->new( host => 'localhost' );
        $consumer = Kafka::Consumer->new( Connection  => $connection );
        my $messages = $consumer->fetch('mytopic',0,0);
        if ( $messages ) {
        foreach $message ( @$messages ) {
		if ( $message->offset == 0 ){
		$BASEURL = $message->payload;
		}
		}
		}
		our $USERAGENT;
		my %TRAVERSED;  #Keeps track of all traversed URLs		
		my $messages1 = $consumer->fetch('mytopic',0,0);
		if ( $messages1 ) {
        foreach my $message1 ( @$messages1 ) {
					if ( $message1->valid ) {		
					our $kafkaurl = $message1->payload;
					my ($domain) = ($BASEURL =~ m#(https|http)://(.+?)/?$#);
					my $RECORDFILE = "ERRORS.$domain.txt";
					$USERAGENT = new LWP::UserAgent;
					open OUT, ">$RECORDFILE";
					extractLinks ($kafkaurl);
					close OUT;
					sub extractLinks{
					my ($url, $containingURL) = @_;
       				my $parse = HTML::LinkExtor -> new ();
       				my $response = $USERAGENT->request(HTTP::Request->new(GET => $url));
					if (!($response -> is_success)){
					print OUT "Stale URL: $url\nContaining URL: $containingURL\nHTTP Message: ", $response->message, "\n\n";
               		return;
                     }
					my $kafkaurlcontent = $response -> content;
					$self->emit( [$kafkaurl, $kafkaurlcontent] );
                    }
					}
                    else {
					say 'error      : ', $message->error;
					}
			    }
				}
				}
		catch {
        my $error = $_;
        if ( blessed( $error ) && $error->isa( 'Kafka::Exception' ) ) {
            warn 'Error: (', $error->code, ') ',  $error->message, "\n";
            exit;
        } else {
            die $error;
        }
    };
    # Closes the consumer and cleans up
    undef $consumer;
    $connection->close;
    undef $connection;
    }
KafkaTestSpout->new->run;
