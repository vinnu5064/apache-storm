#!/usr/bin/perl

package DBSave;

use strict;
use LWP::UserAgent;
use HTML::LinkExtor;
use WWW::Mechanize;
use URI;
use warnings;
use Proc::CPUUsage;
use Memory::Stats;
use Time::HiRes qw( time );
use DBI;

use Moo;
use namespace::clean;

extends 'IO::Storm::Bolt';

#my $cont = WWW::Mechanize->new();
#$cont->get($newURL);
#my $page = $cont->dump_text();
#my $dbh = DBI->connect("DBI:mysql:database=webcrawl;host=localhost", "root", "passwd", {'RaiseError' => 1});
#$dbh->do("INSERT INTO sitecrawl VALUES(?, ?)",  undef, $newURL, $cont->text());
#$dbh->disconnect();



sub process {
my ($self, $tuple) = @_;
my @words = $tuple->values->[0];
foreach my $newURL (@words) {

my $cont = WWW::Mechanize->new();
$cont->get($newURL);
#my $page = $cont->dump_text();
my $dbh = DBI->connect("DBI:mysql:database=webcrawl;host=localhost", "root", "passwd", {'RaiseError' => 1});
$dbh->do("INSERT INTO sitecrawl VALUES(?, ?)",  undef, $newURL, $cont->text());
$dbh->disconnect();
}
}

DBSave->new->run;
