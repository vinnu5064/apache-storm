#!/usr/bin/perl

package UrlGen;

use strict;
use LWP::UserAgent;
use HTML::LinkExtor;
use WWW::Mechanize;
#use URI;
use warnings;
use Proc::CPUUsage;
use Memory::Stats;
use Time::HiRes qw( time );
use URI::URL;

use Moo;
use namespace::clean;

extends 'IO::Storm::Spout';

my $MemoryUsage = Memory::Stats->new();
$MemoryUsage->start;
$MemoryUsage->checkpoint('starting of code Execution'); ###It Records the Memory Usage of script
my $cpu = Proc::CPUUsage->new;
my $CPUusage1 = $cpu->usage(); ##it  returns  cpu  usage since new()
#print $CPUusage1."\n";
my $start = time();

$" = "\n";
my $MAXURLCOUNT = 10000; #The max no of unique URLs to look at
my $COUNT = 0;
my $USERAGENT;
my %TRAVERSED;  #Keeps track of all traversed URLs
my $BASEURL = "http://www.test.local/";
my $MIMEEXTS = "(s?html?|php|xml|asp|pl|css|jpg|gif|pdf)";
my ($domain) = ($BASEURL =~ m#(https|http)://(.+?)/?$#);
my $RECORDFILE = "ERRORS.$domain.txt";

our $url;
our $newURL;

$USERAGENT = new LWP::UserAgent;
open OUT, ">$RECORDFILE";
#extractLinks ($BASEURL);
#next_tuple ($BASEURL);
#UrlGen->new->run($BASEURL);
close OUT;

#####################

sub extractLinks{
#sub next_tuple{
	my ($self, $url, $containingURL) = @_;
	#$url = $self;
	exit if $TRAVERSED{$url};
	$TRAVERSED{$url}++;
	$COUNT = $COUNT + 1;
	my $parse = HTML::LinkExtor -> new ();
	
	my $u1 = new URI::URL($url);
	my $u2 = new HTTP::Request('GET', $u1);
	
	#my $response = $USERAGENT->request(HTTP::Request->new(GET => $url));
	my $response = $USERAGENT->request($u2);
	
	if (!($response -> is_success)){
	#print OUT "Stale URL: $url\nContaining URL: $containingURL\nHTTP Message: ", $response->message, "\n\n";
	return;
	}
	my $file = $response -> content;
	$parse -> parse ($file);
	my @links = $parse->links();
	my ($aLinkRef, %linkHash, @linkArray);
		foreach $aLinkRef (@links){
				my ($tag, $attr, $relativeUrl) = @$aLinkRef;    
				my $absoluteURL = new URI ($relativeUrl);
				$absoluteURL = $absoluteURL->abs($url);    ##It makes relative URL into Absolute URL
				$linkHash{$absoluteURL}++;
				}
	@linkArray = sort (keys %linkHash);
#	print "Total Number Of Links in this page:".@linkArray."\n";

	foreach my $newURL (@linkArray){
		next if $TRAVERSED{$newURL};
		my $givenurl = $BASEURL;
		$givenurl =~ s/(http|https)\:\/\///;
		if ($newURL =~ m/^(https|http)\:\/\/$givenurl/){
			#UrlGen->new->run;
			#sub next_tuple {
 			#my ($self) = @_;
			#print("newURL is $newURL\n");
			#$self->emit( [ $newURL ] );
			
			return $newURL;
				
			#	print "LOOKING INTO RELATIVE URL'S OF IN-DOMAIN #$COUNT:".$newURL."\n";
			
			#  extractLinks ($newURL, $url);
			#	next_tuple ($newURL, $url);
			#UrlGen->new->run($newURL, $newURL, $url);
				}	
				
		else {
			checkLink ($newURL, $url);
		     }
		}		
}	
sub checkLink{
	my ($url, $containingURL) = @_;
	exit if $TRAVERSED{$url};
	$TRAVERSED{$url}++;
	$COUNT = $COUNT + 1;
#	print "Looking at an out-of-domain URL #$COUNT: $url\n";
}


sub next_tuple { 
    my ($self) = @_;
  #  do
#{	
    $self->emit( [ extractLinks($BASEURL, $BASEURL, $url) ] );
	#$BASEURL = $newURL;	
#}while( $newURL eq ' ' );
}
UrlGen->new->run();
