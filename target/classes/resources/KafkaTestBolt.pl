#!/usr/bin/perl
package KafkaTestBolt;
use strict;
use LWP::UserAgent;
use LWP::Simple;
use HTML::LinkExtor;
use HTML::Parse;
use HTML::Element;
use WWW::Mechanize;
use URI;
use DBI;
use warnings;
use Moo;
use namespace::clean;
use Path::Class;
use IO::File;
use Scalar::Util qw(blessed);
use Try::Tiny;
use Kafka::Connection;
use Kafka::Producer;
use Kafka::Consumer;
use HTTP::Request;

extends 'IO::Storm::Bolt';
sub process {
my ($self, $tuple) = @_;
my $url = $tuple->values->[0];
my $urlcontent = $tuple->values->[1];
my $dbh = DBI->connect("DBI:mysql:database=webcrawl;host=localhost", "root", "passwd", {'RaiseError' => 1});
#$dbh->do( "INSERT INTO sitecrawl (URLs,content) VALUES ('$url', '$urlcontent') ");
$dbh->do("INSERT INTO sitecrawl VALUES(?, ?)",  undef, $url, $urlcontent);
#$dbh->disconnect();

try {

my ( $connection, $producer, $consumer, $parse, $BASEURL, @links, @links1, @linkArray, $USERAGENT, $response, $file, $dbh1, $sth1, $field1 );
$connection = Kafka::Connection->new( host => 'localhost' );
$producer = Kafka::Producer->new( Connection => $connection );
$consumer = Kafka::Consumer->new( Connection  => $connection );
my $messages = $consumer->fetch('mytopic',0,0);
if ( $messages ) {
foreach my $message ( @$messages ) {
if ( $message->offset == 0) {
$BASEURL = $message->payload;
}
}
}
		my $MIMEEXTS = "(s?html?|php|xml|asp|pl|css|jpg|gif|pdf)";		
		my $messages1 = $consumer->fetch('mytopic',0,0);
		my $parsed_html = HTML::Parse::parse_html($urlcontent);
		for (@{ $parsed_html->extract_links( ) }) {
		my $link = $_->[0];
		my $i = index($link, '../');
		if($i >= 0)
		{
		$link = substr($link, $i+3);
		}
		my $absoluteURL = new URI ($link);
		$absoluteURL = $absoluteURL->abs($BASEURL);
		push @linkArray, $absoluteURL;
		}

$dbh1 = DBI->connect('DBI:mysql:URLs', 'root', 'passwd') or die "Couldn't open database: $DBI::errstr; stopped";
$sth1 = $dbh1->prepare(<<End_SQL) or die "Couldn't prepare statement: $DBI::errstr; stopped";
SELECT * FROM urls
End_SQL
$sth1->execute() or die "Couldn't execute statement: $DBI::errstr; stopped";
our $ispresent = 0;

		foreach our $newURL (@linkArray){
        my $givenurl = $BASEURL;
        $givenurl =~ s/(http|https)\:\/\///;
		if ($newURL =~ m/^(https|http)\:\/\/$givenurl/){
        foreach my $message1 ( @$messages1 ) {
		if($newURL =~ m/((jpeg|jpg|png|tif|bmp|\#)|\/)$/){
        $ispresent = 1;
		last;
        }
        else
		{
		while ( $field1 = $sth1->fetchrow_array() ){
		if ( $newURL eq $field1 )
		{
		$ispresent = 1;
		last;
		}
		}
		}
		last;
		}
		if(!($ispresent))
		{
		$dbh1->do("INSERT INTO urls VALUES(?)",  undef, $newURL);
		$response = $producer->send('mytopic', 0, "$newURL" );
		}
		}
		}
		}
catch {
        my $error = $_;
        if ( blessed( $error ) && $error->isa( 'Kafka::Exception' ) ) {
            warn 'Error: (', $error->code, ') ',  $error->message, "\n";
            exit;
        } else {
            die $error;
        }
    };
}
KafkaTestBolt->new->run;