package RandomValueSpout;

use Moo;
use namespace::clean;

extends 'IO::Storm::Spout';

my $test = 100;

sub next_tuple {
    my ($self) = @_;
	$self->emit( [ int(rand($test)) ] );
}
RandomValueSpout->new->run();
