package SplitSentenceBolt;

use Moo;
use namespace::clean;

extends 'IO::Storm::Bolt';

sub process {
    my ($self, $tuple) = @_;

my @words = split(' ', $tuple->values->[0]);
   foreach my $word (@words) {
        print("This is a random word  generated from random sentences\n");
        $self->emit([$word]);
  }

}


SplitSentenceBolt->new->run;
